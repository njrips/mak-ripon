import { ref } from 'vue'
export default {
  setup() {
    const count = ref(0)
    return { count }
  },
  template: `<div class="icon-bar">
    <a href="#page-1" class="dot-button"><i class="fa fa-dot-circle-o"></i></a>
    <a href="#page-2" class="dot-button"><i class="fa fa-dot-circle-o"></i></a>
  </div>

  <div class="container-fluid" id="page-1">
    <div class="row content">
      <div class="col-sm-4 sidebar">
        <br />
        <img src="ripon.jpg" class="img-circle" alt="Cinque Terre" width="70%">

        <h3>MOHAMMAD ABDUL KADER (RIPON)</h3>
        <h4><b>M.A.K. Ripon</b></h4>
        <h5><b><i>SOFTWARE ENGINEER</i></b></h5>
        <br />
        <hr class="profile-hr">
        <br />
        <h4 class="profile-text"><b>PROFILE</b></h4>
        
        <p>An experienced software developer/ analyst and professional lead software engineer who is committed to continuous improvement. Capable of working under pressure, both as a project and system manager. Whilst skilled in meeting and exceeding demanding targets.</p>

        <p>Seven years of experience in the software area has taught me a great deal about resilience, adaptation, and transformation.</p>

        <p>Having gained invaluable experience in software development, system analyst, and server maintenance in a professional environment in a high profile group of company and software industries. I am driven by a fervent work ethic and a genuine desire for self-improvement.</p>

        <p>Working in JCL Group as Manager Of Software Department and Assistant Manager of IT Department at Fashion Flash Limited, Fashion Jeans Limited, JCL Wash Limited, Denim Attires Limited, Denim Art Limited, Right Link Industries, and Creative Tiles Industries.</p>

        <br><br><br>
        <br><br><br>
        <hr class="profile-hr">
        <h4 class="profile-text"><b>R<small class="profile-text">EACH ME AT</small></b></h4>
        <p>+880 1911 302 ___<br>Uttara, Dhaka</p>
        <p>njrips@gmail.com</p>
      </div>

      <div class="col-sm-8">
        <div class="well">
          <h4><big><b>CORE SKILLS</b></big></h4>
          <div class="row">
            <div class="col-sm-6">
              <ul>
                <li><b>Data Structures and Algorithms</b></li>
                <li><b>Software Development</b></li>
                <li><b>Object-Oriented Design</b></li>
                <li><b>Software Testing and Debugging</b></li>
                <li><b>Problem Solving and Logical Thinking</b></li>
                <li><b>Teamwork</b></li>
                <li><b>Multitasking</b></li>
                <li><b>Data Analysis</b></li>
                <li><b>Critical Thinking</b></li>
                <li><b>Creativity</b></li>
              </ul>
            </div>
            <div class="col-sm-6">
              <ul>
                <li><b>JavaScript</b></li>
                <li><b>ReactJS</b></li>
                <li><b>NodeJS</b></li>
                <li><b>SQL</b></li>
                <li><b>PHP</b></li>
                <li><b>Android</b></li>
                <li><b>Java</b></li>
                <li><b>C++ / C</b></li>
                <li><b>Python</b></li>
                <li><b>Ruby</b></li>
                <li><b>HTML / CSS</b></li>
              </ul>
            </div>
          </div>
          <br>
          <h4><b>EMPLOYMENT HISTORY</b></h4><hr style="border: 2px solid black;">
          <h5><b><big>Assistant Manager (Software & IT)</big></b></h5>
          <h6><b><big>JCL GROUP</big></b></h6>
          <h6><b><big>SEPTEMBER 2015 - DATE</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>I’ve developed a full workable ERP Solution for Apparel Industry.</b></li>
                <li><b>Store & Inventory management system module that is integrated with ERP Solution.</b></li>
                <li><b>Developed a Visual Drag & Drop Planning Board for order line management which creates fast and efficient production with fewer IE employees.</b></li>
                <li><b>Full HR management system controlling from attendance to salary automation actively running in 8 factories.</b></li>
                <li><b>Account & Commercial management system module that is integrated with ERP Solution.</b></li>
                <li><b>Moinuddin Memorial College (MMC) Account & Management System.</b></li>
                <li><b>User Access Control System to manage all other software access with internal chat, mailing, work dashboard system.</b></li>
                <li><b>Created and managing our group mail server.</b></li>
                <li><b>Android app for MD to get all summarize data and graphs.</b></li>
                <li><b>17 different websites including or excluding company with mostly WordPress, Laravel, and CodeIgniter.</b></li>
                <li><b>I’ve to also look after the IT employees and machineries including finger punch machines for the group sister companies. Also, have to maintain the HR rules and look after it for the group central.</b></li>
                <li><b>From data collection to data analysis, report requirement taking to report development, implementation to maintenance, and debugging has been done.</b></li>
                <li><b>Tools: NodeJS (Express), Javascript, ExtJS (Most FrontEnd), Laravel, Bootstrap, CodeIgniter, and HTML / CSS.</b></li>
              </ul>
            </div>
          </div>
          <h5><b><big>Senior Software Developer</big></b></h5>
          <h6><b><big>SoltechBD</big></b></h6>
          <h6><b><big>APRIL 2014 - AUGUST 2015</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>Green Grade Solutions UpSkill project for compliance online exam certification system for web-based on Ruby on Rails and android app.</b></li>
                <li><b>Bitcoin pay per click add site like btcclicks.com with Symfony Framework.</b></li>
                <li><b>Creative Tiles Industry dynamic 3D website for floor planning and price calculating with YII framework.</b></li>
                <li><b>Creative Tiles Industry stock and inventory management system with NodeJS & ExtJS.</b></li>
                <li><b>Dyeing and Washing  Industry management system with NodeJS & ExtJS.</b></li>
                <li><b>Fire Controller system PCB Board Design with Proteus and coding with C.</b></li>
              </ul>
            </div>
          </div>
          <h5><b><big>Software Developer</big></b></h5>
          <h6><b><big>Iris Technology Bangladesh</big></b></h6>
          <h6><b><big>January 2014 - March 2014</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>Sazoo Online Shoe Store 3D product view application with CodeIgniter and JQuery.</b></li>
                <li><b>Iristechbd.com Website with CodeIgniter.</b></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <div class="container-fluid" id="page-2">
    <div class="row content">
      <div class="col-sm-4 sidebar">
        <br><br><br><br>
        <br><br><br><br>
        <h4 class="profile-text"><b>PERSONAL INFORMATION</b></h4><hr style="border: 2px solid white;">
        <ul>
          <li><b>Date of Birth - December 01, 1991</b></li>
          <li><b>Marital Status: Married</b></li>
          <li><b>Permanent Address: C/O – MD. Yakub Patoary House# 20, Road# 06, Block- E, P.S# Turag, P.O# Nishatnagar, Uttara, Dhaka-1230</b></li>
        </ul>
        <br><br><br><br>
        <h4 class="profile-text"><b>INTERESTS AND ACTIVITIES</b></h4><hr style="border: 2px solid white;">
        <p>I am a people person who loves socializing, sports, gaming and traveling.</p>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
        <br><br><br><br>
      </div>

      <div class="col-sm-8">
        <div class="well">
          <h4><b>EMPLOYMENT HISTORY</b></h4><hr style="border: 2px solid black;">
          <h5><b><big>Programmer</big></b></h5>
          <h6><b><big>Insert Technologies</big></b></h6>
          <h6><b><big>July 2012 - December 2013</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>Bangladesh Medical & Dental Council full doctors registration and certification system with Zend Framework.</b></li>
                <li><b>Melange Restaurant order and bill payment system.</b></li>
                <li><b>General Store billing and inventory system.</b></li>
                <li><b>bdhangouts.com Website design with raw PHP.</b></li>
                <li><b>Inserttech.com Website design with raw PHP.</b></li>
              </ul>
            </div>
          </div>
          <h5><b><big>Project Supervisor</big></b></h5>
          <h6><b><big>Intelligent Image Management Ltd.</big></b></h6>
          <h6><b><big>February 2012 - June 2012</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>Google Street View large-scale privacy protection image blurring project quality checker.</b></li>
                <li><b>Advocacy Book OCR converting project supervisor.</b></li>
              </ul>
            </div>
          </div>
          <h5><b><big>Seo Specialist</big></b></h5>
          <h6><b><big>Egen Solutions</big></b></h6>
          <h6><b><big>March 2008 - August 2010</big></b></h6>
          <div class="row">
            <div class="col-sm-12">
              <ul>
                <li><b>Affiliates Marketing, Blogging, SEO, and Forex Monitoring.</b></li>
                <li><b>MLM website customization with Joomla.</b></li>
                <li><b>WordPress website customization.</b></li>
              </ul>
            </div>
          </div>
          <h4><b>EDUCATION</b></h4><hr style="border: 2px solid black;">
          <h5><b><big>Southeast University</big></b></h5>
          <h6><b><big>BSC in CSE (Computer Science & Engineering) - 2013</big></b></h6>
          <br>
          <h5><b><big>Milestone College</big></b></h5>
          <h6><b><big>Higher Secondary Certificate (H. S. C) - Science - 2008</big></b></h6>
          <br><br><br><br><br><br>
          <br><br><br><br><br><br>
          <br><br><br><br><br><br>
          <br><br><br><br><br>
        </div>
      </div>
    </div>
  </div>`
}